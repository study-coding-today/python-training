
# set() 집합 중복허용안함 단. 순서 무작위임
a = ["1", 1, "1", 2]
a = list(set(a))
print(a)

print()
print()

# 딕션너리의 key : value 형태로 순서대로 적용
from collections import OrderedDict
a = ["1", 1, "1", 2]
a = OrderedDict.fromkeys(a)
print(a)
a = list(a)
print(a)

print()
print()

# 순서대로 적용되나 두 list를 모두 확인하며 
# 데이터가 많을 경우 시간이 많이 걸린다.
a = ["1", 1, "1", 2]
b = []
for i in a:
    if not i in b:
        b.append(i)
print(b)

